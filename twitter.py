#!/bin/python2
# -*- coding: utf8 -*-
import csv, random, argparse

class TwitterClassifier:
	def __init__(self):
		train_file = open('data/twitter-train.csv', 'r')
		self.train_reader = csv.reader(train_file)

		test_in_file = open('data/twitter-test.csv', 'r')
		self.test_reader = csv.reader(test_in_file)

		# Skip the header row
		self.train_reader.next()
		self.test_reader.next()

	def train(self):
		## Your training logic goes here
		pass

	def predict(self):
		test_out_file = open('result/twitter-test.csv', 'w')
		self.test_writer = csv.writer(test_out_file, quoting=csv.QUOTE_ALL)

		self.test_writer.writerow(['review', 'rating'])

		for entry in self.test_reader:
			## Your prediction logic goes here
			prediction = ','.join(random.sample([u'😂', u'😍', u'❤', u'🎉', u'😀'], random.randint(1,5)))
			self.test_writer.writerow([entry[0], prediction.encode('utf8')])

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--train', action='store_true')
	parser.add_argument('--test', action='store_true')

	args = parser.parse_args()

	trainer = TwitterClassifier()

	if args.train:
		print('Training...')
		trainer.train()

	if args.test:
		print('Testing...')
		trainer.predict()
