# E0 334 Assignment 2

The deadline for the first part of this assignment is **Monday, September 17, 2018** (excluding the `-autoencoder` files).  
The deadline for the second part is **Monday, October 1, 2018** (including the `-autoencoder` files).

In this assignment, you are tasked with implementing a preprocessing and classification pipeline for 3 datasets, namely:

1. The [Large Movie Review Dataset](http://ai.stanford.edu/~amaas/data/sentiment/) (IMDB).
2. The [Stanford Natural Language Inference Corpus](https://nlp.stanford.edu/projects/snli/) (SNLI).
3. Twitter Emoji Classification (Twitter), based on the [Archive Team Twitter Stream Grab](https://archive.org/details/twitterstream).

For the purposes of this assignment, you are expected to use `word2vec` representations along with the classifiers as discussed in class.  
The first two datasets are **unchanged** from before, but the skeleton `.py` files have changed.

Note that the Twitter dataset is sparsely multiclass. Multiple emoji may appear in the `emoji` column, separated by commas.

## Evaluation
For the first two datasets, report the metric used is Accuracy.  
For the emoji dataset, the metric used will be a per-class *Precision*, *Recall* and *F1 Score*.

## Getting Started

1. Please start by forking this repository (tick the checkbox for `This is a private repository`). This should result in the creation of a new repository under your account.
2. Edit the three Python files to implement your solution. In case you are using Python 3, please update the Shebang (`#!`)  to reflect this.
3. Follow the submission instructions to share your solution with us. **Do not submit a Pull Request, unless you want to suggest a fix.**

## Submission Instructions

In your submission, include a `readme.md` with basic instructions and a `requirements.txt` that includes a complete list of packages (and their corresponding versions) that need to be installed. If you make use of any additional data, such as `nltk` packages, you must download them in your code using `nltk.download(...)`. The packages specified will be installed using `pip install -r requirements.txt`, so do not include your Python version, or anything that may not be found by pip. **Furthermore, do not copy this readme as is, write your own; and format it in markdown.** Detail your results on the validation set, feature engineering and feature selection strategies and any ablation studies you might have performed separately in a `report.pdf` file. It should be possible to run your code on a fresh `virtualenv` after installing the specified packages without any other set-up. We will install the specified packages using `pip`, and then run the three Python scripts to check your solution as follows:

    python imdb.py --test
    python snli.py --test
    python twitter.py --test
    python imdb-autoencoder.py --test
    python twitter-autoencoder.py --test

Please include your trained weights and load them automatically. You can use Git LFS to store the weights (up to 1GB).  

To submit your solution, go to `Settings > User and group access` and add `peteykun` with `Read` access to the `Users` section. We will automatically retrieve the latest submission via git.
